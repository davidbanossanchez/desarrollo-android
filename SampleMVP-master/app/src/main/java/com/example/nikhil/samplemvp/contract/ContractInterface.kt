package com.example.nikhil.samplemvp.contract

interface ContractInterface {

    interface View {
        fun initView()
        fun updateViewData()
    }

    interface Presenter {
        fun incrementValue()
        fun getCounter(): String
        fun getMessages()
    }

    interface Model {
        fun getCounter(): Int
        fun incrementCounter()
        fun getMessage()
    }

}