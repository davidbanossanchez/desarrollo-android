package com.example.nikhil.samplemvp.model


import com.example.nikhil.samplemvp.contract.ContractInterface.*

class MainActivityModel : Model {

    private var mCounter = 0

    override fun getCounter() = mCounter

    override fun incrementCounter() {
        mCounter++
    }

    override fun getMessage() {
        System.out.println("El modelo es : : : : :")
        var arrayData = ArrayList<String>()
        arrayData.add("1")
        arrayData.add("2")
        arrayData.add("1")
        arrayData.add("1")
        arrayData.add("3")
        println("El arreglo es: " + arrayData)
        println("tipo arreglo: " + arrayData.toList().distinct())
        var arre = arrayData.toList().distinct()
        var arreglo = ArrayList(arre)
        println("Nuevo arreglo: " + arreglo)
    }
}