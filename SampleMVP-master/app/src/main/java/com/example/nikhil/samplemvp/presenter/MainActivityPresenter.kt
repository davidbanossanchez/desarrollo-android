package com.example.nikhil.samplemvp.presenter

import com.example.nikhil.samplemvp.contract.ContractInterface.*
import com.example.nikhil.samplemvp.model.MainActivityModel

class MainActivityPresenter(_view: View): Presenter {
    private var view: View = _view
    //Instanciando el modelo de MainActivityModel
    private var model: Model = MainActivityModel()
    init {
        view.initView()
    }
    override fun incrementValue() {
        //Incrementa
        model.incrementCounter()
        //Muestra la vista
        view.updateViewData()
    }
     override fun getCounter() = model.getCounter().toString()
    override fun getMessages() = model.getMessage()

}